FROM python:3-alpine

WORKDIR /app

COPY app.py .

EXPOSE 80

CMD ["python", "app.py"]